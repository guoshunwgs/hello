package com.guoshun.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName : DemoApplication
 * @Description :
 * @Author : guoshunwgs
 * @Date: 2020-05-11 22:43
 */
@SpringBootApplication
public class DemoApplication
{
    public static void main(String[] args)
    {

        SpringApplication.run(DemoApplication.class, args);
    }
}
