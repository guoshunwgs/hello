package com.guoshun.study;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : HelloController
 * @Description :
 * @Author : guoshunwgs
 * @Date: 2020-05-11 22:51
 */
@RestController
public class HelloController

{
    @GetMapping("/hello")
    public  String hello(){
        return "guoshun test demo ";
    }
}
